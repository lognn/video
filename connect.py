# -*- codeing = utf-8 -*-
# @Time : 2021/12/4 8:59
# @Author : Lognn
# @File : connect.py
# @Software : PyCharm
import pymysql


def mysql_select(execute):
    conn = pymysql.connect(host='localhost', port=3306, user='luogan',
                           password='lognn1', database='lognn',
                           charset='utf8mb4')
    itme_list = []
    try:
        with conn.cursor(cursor=pymysql.cursors.DictCursor) as cursor:
            affected_rows = cursor.execute('%s' % execute)
            for rows in cursor.fetchall():
                itme_list.append(rows)
    except pymysql.MySQLError as err:
        print(err)
        # 操作失败。回滚
        conn.rollback()
    finally:
        # 关闭数据库连接
        cursor.close()
        conn.close()
    return itme_list


def mysql_insert(execute):
    return_value = ''
    conn = pymysql.connect(host='localhost', port=3306, user='luogan',
                           password='lognn1', database='lognn',
                           charset='utf8mb4')
    try:
        with conn.cursor(cursor=pymysql.cursors.DictCursor) as cursor:
            affected_rows = cursor.execute('%s' % execute)
            if affected_rows == 1:
                return_value = '操作成功'
            else:
                pass
    except pymysql.MySQLError as err:
        print(err)
        # 操作失败。回滚
        conn.rollback()
    finally:
        # 关闭数据库连接
        cursor.close()
        conn.close()
    return return_value

