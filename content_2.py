# -*- codeing = utf-8 -*-
# @Time : 2021/12/14 22:35
# @Author : Lognn
# @File : content_2.py
# @Software : PyCharm
import re
import urllib.request
import bs4
import connect
import jieba
import datetime


def geturl():
    for i in range(1,2):
        url = 'https://www.ziyuanwu.com/se/teaching/page/' + str(i)
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36'
        }
        try:
            request = urllib.request.Request(url=url, headers=headers)
            response = urllib.request.urlopen(request)
            html = response.read().decode('utf-8')
        except urllib.error.URLError as e:
            if hasattr(e, "code"):
                print(e.code)
            if hasattr(e, 'reason'):
                print(e.reason)
        bs = bs4.BeautifulSoup(html, 'html.parser')
        for itme in bs.find_all(class_="post-loop post-loop-card cols-3"):
            itme = str(itme)
            con_links = re.findall(re.compile(r'item-wrap" href="(.*?)" rel="bookmark" target='), itme)
            for con_link in con_links:
                try:
                    request = urllib.request.Request(url=con_link, headers=headers)
                    response = urllib.request.urlopen(request)
                    html = response.read().decode('utf-8', errors='ignore')
                except urllib.error.URLError as e:
                    if hasattr(e, "code"):
                        print(e.code)
                    if hasattr(e, 'reason'):
                        print(e.reason)
                bs = bs4.BeautifulSoup(html, 'html.parser')
                video_time = datetime.date.today()
                title = bs.find(class_="entry-title").string
                if len(re.findall(re.compile(r'</h3>\s+<p>(.*?)</p>.*?\s+<h\d>'), str(bs))) > 0:
                    itme = ''.join(re.findall(re.compile(r'</h3>\s+<p>(.*?)</p>.*?\s+<h\d>'), str(bs))[0].split())
                    if len(re.findall(re.compile(r'rel="category tag">(.*?)</a>'), str(bs))) > 0:
                        video_class = re.findall(re.compile(r'rel="category tag">(.*?)</a>'), str(bs))[0]
                        if len(re.findall(re.compile(r'<a href="(https://pan.baidu.com/s/.*?)" rel='), str(bs))) > 0:
                            bd_url = re.findall(re.compile(r'<a href="(https://pan.baidu.com/s/.*?)" rel='), str(bs))[0]
                        else:
                            bd_url = ''
                        if len(re.findall(re.compile(r'<a href="(https://cloud.189.cn/t/.*?)" rel='), str(bs))) > 0:
                            ty_url = re.findall(re.compile(r'<a href="(https://cloud.189.cn/t/.*?)" rel='), str(bs))[0]
                        else:
                            ty_url = ''
                        if len(re.findall(re.compile(r'<a href="(https://www.aliyundrive.com/.*?)" rel='), str(bs))) > 0:
                            al_url = re.findall(re.compile(r'<a href="(https://www.aliyundrive.com/.*?)" rel='), str(bs))[0]
                        else:
                            al_url = ''
                        if len(re.findall(re.compile(r'src="(https://img.4rz.cn/ziyuanwu/wp-content/uploads/.*?.jpg)" width='), str(bs))) > 0:
                            video_img = re.findall(re.compile(r'src="(https://img.4rz.cn/ziyuanwu/wp-content/uploads/.*?.jpg)" width='), str(bs))[0]
                        else:
                            video_img = ''
                        video_tags = ' '.join(jieba.cut(title))
                        execute = 'insert into video (name,up_time,video_db,video_class,tags,bd_url,ty_url,al_url,video_img) values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' % (
                        title, video_time, itme,video_class, video_tags, bd_url, ty_url, al_url,video_img)
                        if len(connect.mysql_select('SELECT name FROM video WHERE name="%s"' % title)) > 0:
                            print(str(i) + '重复数据，跳过',datetime.datetime.now())
                        else:
                            print(str(i) + connect.mysql_insert(execute=execute),datetime.datetime.now())
                    else:
                        print('err,定位数据失败.')
                else:
                    print('err,定位数据失败.')


if __name__ == '__main__':
    geturl()
