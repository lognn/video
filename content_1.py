# -*- codeing = utf-8 -*-
# @Time : 2021/12/11 21:34
# @Author : Lognn
# @File : content.py
# @Software : PyCharm
import re
import urllib.request
import bs4
import connect
import jieba
import datetime


def geturl():
    for i in range(1,2):
        url = 'https://www.x6g.com/html/18-' + str(i) +'.html'
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36'
        }
        try:
            request = urllib.request.Request(url=url, headers=headers)
            response = urllib.request.urlopen(request)
            html = response.read().decode('utf-8')
        except urllib.error.URLError as e:
            if hasattr(e, "code"):
                print(e.code)
            if hasattr(e, 'reason'):
                print(e.reason)
        bs = bs4.BeautifulSoup(html, 'html.parser')
        for itme in bs.find_all(class_="list-info"):
            itme = str(itme)
            con_link = 'https://www.x6g.com' + re.findall(re.compile(r'<a href="(.*?)" target="_blank">立即查看</a>'),itme)[0]
            try:
                request = urllib.request.Request(url=con_link, headers=headers)
                response = urllib.request.urlopen(request)
                html = response.read().decode('utf-8',errors='ignore')
            except urllib.error.URLError as e:
                if hasattr(e, "code"):
                    print(e.code)
                if hasattr(e, 'reason'):
                    print(e.reason)
            bs = bs4.BeautifulSoup(html, 'html.parser')
            title = bs.find(class_="article-title").string
            video_time = re.findall(re.compile(r'<time><i class="iconfont icon-time"></i>(.*?)\d+:\d+</time>'),str(bs))[0].strip()
            itme = str(bs.find(class_="article-content"))
            video_class = str(bs.find(class_="bq-wg"))
            if len(re.findall(re.compile(r'课程介绍</.*?><p>(.*?)</p><.*?>学习地址'),itme)) == 1:
                aiticle_content = re.findall(re.compile(r'课程介绍</.*?><p>(.*?)</p><.*?>学习地址'),itme)[0].replace('"'," ")
                if len(re.findall(re.compile(r'<a href="(https://pan.baidu.com/.*?)" target="_blank'),itme)) == 1 :
                    bd_url = re.findall(re.compile(r'<a href="(https://pan.baidu.com/.*?)" target="_blank'), itme)[0]
                else:
                    bd_url = ''
                if len(re.findall(re.compile(r'title="本文所属栏目：.*?">(.*?)</a></b>'), video_class)) == 1:
                    video_class = re.findall(re.compile(r'title="本文所属栏目：.*?">(.*?)</a></b>'), video_class)[0]
                else:
                    video_class = ''
                if len(re.findall(re.compile(r'<a href="(https://cloud.189.cn/.*?)" target="_blank'),itme)) == 1:
                    ty_url = re.findall(re.compile(r'<a href="(https://cloud.189.cn/.*?)" target="_blank'), itme)[0]
                else:
                    ty_url = ''
                if len(re.findall(re.compile(r'<a href="(https://www.aliyundrive.com/.*?)" target="_blank'),itme)) ==1:
                    al_url = re.findall(re.compile(r'<a href="(https://www.aliyundrive.com/.*?)" target="_blank'), itme)[0]
                else:
                    al_url = ''
                if len(re.findall(re.compile(r'src="(/uploads/allimg/.*?)" title='), itme)) ==1:
                    video_img = 'https://x6g.com' + re.findall(re.compile(r'src="(/uploads/allimg/.*?)" title='), itme)[0]
                else:
                    video_img = ''
                video_tags = ' '.join(jieba.cut(title))
                execute = 'insert into video (name,up_time,video_db,video_class,tags,bd_url,ty_url,al_url,video_img) values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' % (
                title, video_time, aiticle_content,video_class, video_tags, bd_url, ty_url, al_url, video_img)
                if len(connect.mysql_select('SELECT name FROM video WHERE name="%s"' % title)) > 0:
                    print(str(i) + '重复数据，跳过',datetime.datetime.now())
                else:
                    print(str(i) + connect.mysql_insert(execute=execute),datetime.datetime.now())
            else:
                print('err,定位数据失败.')



if __name__ == '__main__':
    geturl()
