# -*- codeing = utf-8 -*-
# @Time : 2021/12/12 17:17
# @Author : Lognn
# @File : main.py
# @Software : PyCharm
import datetime
import re

from flask import Flask, render_template, request
import connect
app = Flask(__name__)


@app.route('/')
def index():
    id = 1
    execute = 'SELECT * FROM video order by Id desc;'
    total = int(len(connect.mysql_select('SELECT * FROM video')))
    content_list = connect.mysql_select(execute=execute)
    cont_list = []
    for itme in content_list:
        Id = str(itme['Id'])
        name = itme['name'][0:20]
        up_time = itme['up_time']
        video_db = itme['video_db'][0:12]
        name_1 = itme['name']
        video_db_1 = itme['video_db']
        video_class = itme['video_class']
        tags = itme['tags']
        if itme['bd_url'] == '':
            bd_url = ''
        else:
            bd_url = itme['bd_url']
            bd_url = '<a href="%s" target="_blank">百度网盘丨</a>' % bd_url
        if itme['ty_url'] == '':
            ty_url = ''
        else:
            ty_url = itme['ty_url']
            ty_url = '<a href="%s" target="_blank">天翼网盘</a>' % ty_url
        if itme['al_url'] == '':
            al_url = ''
        else:
            al_url = itme['al_url']
            al_url = '<a href="%s" target="_blank">丨阿里网盘</a>' % al_url
        content_list = [Id, name, up_time, video_db, video_class, tags, bd_url, ty_url, al_url, '']
        cont_list.append(content_list)
    page = int(len(cont_list) / 30)
    itme_list = []
    num = 1
    for i in range(30):
        cont_list[i][-1] = str(num)
        itme_list.append(cont_list[i])
        num += 1
    return render_template('index.html', itme_list=itme_list, id=id, page=page, name_1=name_1, video_db_1=video_db_1,
                           total=total)


@app.route('/page=<int:id>')
def index1(id):
    execute = 'SELECT * FROM video order by Id desc;'
    content_list = connect.mysql_select(execute=execute)
    cont_list = []
    for itme in content_list:
        Id = str(itme['Id'])
        name = itme['name'][0:20]
        up_time = itme['up_time']
        video_db = itme['video_db'][0:12]
        name_1 = itme['name']
        video_db_1 = itme['video_db']
        video_class = itme['video_class']
        tags = itme['tags']
        if itme['bd_url'] == '':
            bd_url = ''
        else:
            bd_url = itme['bd_url']
            bd_url = '<a href="%s" target="_blank">百度网盘丨</a>' % bd_url
        if itme['ty_url'] == '':
            ty_url = ''
        else:
            ty_url = itme['ty_url']
            ty_url = '<a href="%s" target="_blank">天翼网盘</a>' % ty_url
        if itme['al_url'] == '':
            al_url = ''
        else:
            al_url = itme['al_url']
            al_url = '<a href="%s" target="_blank">丨阿里网盘</a>' % al_url
        content_list = [Id, name, up_time, video_db, video_class, tags, bd_url, ty_url, al_url, '']
        cont_list.append(content_list)
    itme_list = []
    num = 1
    page = int(len(cont_list) / 30)
    if id <= len(cont_list) / 30:
        for i in range(id * 30 - 30, id * 30):
            cont_list[i][-1] = str(num)
            itme_list.append(cont_list[i])
            num += 1
        return render_template('index.html', itme_list=itme_list, id=id, page=page, name_1=name_1,
                               video_db_1=video_db_1)
    else:
        return '溢出...'


@app.route('/wordcloud')
def index2():
    execute = 'SELECT tags FROM video'
    content_list = connect.mysql_select(execute=execute)
    itme_list = []
    for itmes in content_list:
        itmes = itmes['tags'].split(" ")
        for itme in itmes:
            if itme in itme_list:
                pass
            else:
                if len(itme) < 2:
                    pass
                else:
                    itme_list.append(itme)
    return render_template('wordcloud.html', itme_list=itme_list)


@app.route('/category')
def index7():
    execute = 'SELECT video_class FROM video'
    content_list = connect.mysql_select(execute=execute)
    itme_list = []
    for itmes in content_list:
        itmes = itmes['video_class']
        if itmes in itme_list:
            pass
        else:
            itme_list.append(itmes)
    return render_template('category.html', itme_list=itme_list)


@app.route('/s=<name>')
def index3(name):
    execute = 'SELECT * FROM video WHERE tags LIKE "%{0}%" order by Id desc'.format(name)
    content_list = connect.mysql_select(execute=execute)
    cont_list = []
    for itme in content_list:
        Id = str(itme['Id'])
        name = itme['name'][0:20]
        up_time = itme['up_time']
        video_db = itme['video_db'][0:12]
        name_1 = itme['name']
        video_db_1 = itme['video_db']
        video_class = itme['video_class']
        tags = itme['tags']
        if itme['bd_url'] == '':
            bd_url = ''
        else:
            bd_url = itme['bd_url']
            bd_url = '<a href="%s" target="_blank">百度网盘丨</a>' % bd_url
        if itme['ty_url'] == '':
            ty_url = ''
        else:
            ty_url = itme['ty_url']
            ty_url = '<a href="%s" target="_blank">天翼网盘</a>' % ty_url
        if itme['al_url'] == '':
            al_url = ''
        else:
            al_url = itme['al_url']
            al_url = '<a href="%s" target="_blank">丨阿里网盘</a>' % al_url
        content_list = [Id, name, up_time, video_db, video_class, tags, bd_url, ty_url, al_url, '']
        cont_list.append(content_list)
    itme_list = []
    num = 1
    for i in range(len(cont_list)):
        cont_list[i][-1] = str(num)
        itme_list.append(cont_list[i])
        num += 1
    return render_template('s.html', itme_list=itme_list, name_1=name_1, video_db_1=video_db_1)


@app.route('/c=<name>')
def index4(name):
    execute = 'SELECT * FROM video WHERE video_class LIKE "%{0}%" order by Id desc'.format(name)
    content_list = connect.mysql_select(execute=execute)
    cont_list = []
    for itme in content_list:
        Id = str(itme['Id'])
        name = itme['name'][0:20]
        up_time = itme['up_time']
        video_db = itme['video_db'][0:12]
        name_1 = itme['name']
        video_db_1 = itme['video_db']
        video_class = itme['video_class']
        tags = itme['tags']
        if itme['bd_url'] == '':
            bd_url = ''
        else:
            bd_url = itme['bd_url']
            bd_url = '<a href="%s" target="_blank">百度网盘丨</a>' % bd_url
        if itme['ty_url'] == '':
            ty_url = ''
        else:
            ty_url = itme['ty_url']
            ty_url = '<a href="%s" target="_blank">天翼网盘</a>' % ty_url
        if itme['al_url'] == '':
            al_url = ''
        else:
            al_url = itme['al_url']
            al_url = '<a href="%s" target="_blank">丨阿里网盘</a>' % al_url
        content_list = [Id, name, up_time, video_db, video_class, tags, bd_url, ty_url, al_url, '']
        cont_list.append(content_list)
        itme_list = []
    num = 1
    for i in range(len(cont_list)):
        cont_list[i][-1] = str(num)
        itme_list.append(cont_list[i])
        num += 1
    return render_template('s.html', itme_list=itme_list, name_1=name_1, video_db_1=video_db_1)


@app.route('/s', methods=['GET', 'POST'])
def index6():
    if request.method == 'POST':
        value = str(request.form.get('value'))
    execute = 'SELECT * FROM video WHERE name LIKE "%{0}%" order by Id desc'.format(value)
    content_list = connect.mysql_select(execute=execute)
    cont_list = []
    for itme in content_list:
        Id = str(itme['Id'])
        name = itme['name'][0:20]
        up_time = itme['up_time']
        video_db = itme['video_db'][0:12]
        name_1 = itme['name']
        video_db_1 = itme['video_db']
        name_1 = itme['name']
        video_db_1 = itme['video_db']
        video_class = itme['video_class']
        tags = itme['tags']
        if itme['bd_url'] == '':
            bd_url = ''
        else:
            bd_url = itme['bd_url']
            bd_url = '<a href="%s" target="_blank">百度网盘丨</a>' % bd_url
        if itme['ty_url'] == '':
            ty_url = ''
        else:
            ty_url = itme['ty_url']
            ty_url = '<a href="%s" target="_blank">天翼网盘</a>' % ty_url
        if itme['al_url'] == '':
            al_url = ''
        else:
            al_url = itme['al_url']
            al_url = '<a href="%s" target="_blank">丨阿里网盘</a>' % al_url
        content_list = [Id, name, up_time, video_db, video_class, tags, bd_url, ty_url, al_url, '']
        cont_list.append(content_list)
    itme_list = []
    num = 1
    if len(cont_list) > 0:
        for i in range(len(cont_list)):
            cont_list[i][-1] = str(num)
            itme_list.append(cont_list[i])
            num += 1
        return render_template('s.html', itme_list=itme_list, name_1=name_1, video_db_1=video_db_1)
    else:
        return '未找到相关内容...'


@app.route('/<int:id>.html')
def index8(id):
    execute = 'SELECT * FROM video WHERE Id = "{0}"'.format(id)
    content_list = connect.mysql_select(execute=execute)
    for itme in content_list:
        Id = str(itme['Id'])
        name = itme['name'][0:20]
        up_time = itme['up_time']
        video_db = itme['video_db'][0:12]
        name_1 = itme['name']
        video_db_1 = itme['video_db']
        video_class = itme['video_class']
        tags = itme['tags']
        if itme['bd_url'] == '':
            bd_url = ''
        else:
            bd_url = itme['bd_url']
            bd_url = '<div class="col-md-3 col-sm-12"><a href="%s" target="_blank"><img class="img-fluid" src="static/images/baidu.png" alt="百度网盘下载地址" title="百度网盘下载地址"/></a></div>' % bd_url
        if itme['ty_url'] == '':
            ty_url = ''
        else:
            ty_url = itme['ty_url']
            ty_url = '<div class="col-md-3 col-sm-12"><a href="%s" target="_blank"><img class="img-fluid" src="static/images/tianyi.png" alt="天翼网盘下载地址" title="天翼网盘下载地址"/></a></div>' % ty_url
        if itme['al_url'] == '':
            al_url = ''
        else:
            al_url = itme['al_url']
            al_url = '<div class="col-md-3 col-sm-12"><a href="%s" target="_blank"><img class="img-fluid" src="static/images/aliyun.png" alt="阿里云网盘下载地址" title="阿里云网盘下载地址"/></a></div>' % al_url
        if itme['video_img'] == '':
            video_img = ''
        else:
            video_img = itme['video_img']
            #video_img = '<img src="%s" alt="" width="700"/>' % video_img
        content_list = [up_time, video_class, tags, bd_url, ty_url, al_url]
    return render_template('aiticle.html', name_1=name_1, video_db_1=video_db_1, up_time=up_time,
                           video_class=video_class, bd_url=bd_url, ty_url=ty_url, al_url=al_url, video_img=video_img)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)